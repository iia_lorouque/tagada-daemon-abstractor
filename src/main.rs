mod operator_info_store;

use std::{io, thread};
use std::fmt::Debug;
use std::io::{Read, Write};
use std::net::{Shutdown, SocketAddr, TcpListener, TcpStream};
use std::panic::Location;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Receiver};
use std::time::Duration;
use clap::{Parser, command, Subcommand, Args};
use log::{info, warn};
use tagada_structs::differential::{DifferentialFunctionNode};
use tagada_structs::truncated_differential::TruncatedDifferentialFunctionNode;
use crate::operator_info_store::{compute_abstraction_of, OperatorInfoStore};

type Request = DifferentialFunctionNode;
type Response = TruncatedDifferentialFunctionNode;

trait OrLogErr {
    fn or_log_err(&self);
}

impl<T, E> OrLogErr for Result<T, E> where E: Debug {
    fn or_log_err(&self) {
        if let Err(e) = self {
            warn!("{:?} at {} {}:{}", e, file!(), line!(), column!());
        }
    }
}

#[derive(Parser)]
struct Cli {
    #[command(subcommand)]
    mode: Mode,
}

#[derive(Subcommand)]
enum Mode {
    Client(Client),
    Server(Server),
}

#[derive(Args)]
struct Client {
    #[arg(short, long, default_value = "1024")]
    port: u16,
    #[arg(short, long)]
    differential_function: String,
}

#[derive(Args)]
struct Server {
    #[arg(short, long, default_value = "1024")]
    port: u16,
}

fn run_server(port: u16) -> io::Result<()> {
    let listener = TcpListener::bind(SocketAddr::from(([0, 0, 0, 0], port)))?;

    let operator_info_store = OperatorInfoStore::new();
    let (computation_tx, computation_rx) = channel();
    let cache = Arc::new(Mutex::new(operator_info_store));
    let cache_copy = cache.clone();
    thread::spawn(move || { run_computation_loop(computation_rx, cache_copy) });

    while let Ok((mut tcp_stream, _)) = listener.accept() {
        let mut request = Vec::new();
        tcp_stream.set_read_timeout(Some(Duration::from_secs(5)))?;
        if tcp_stream.read_to_end(&mut request).is_err() {
            warn!("An error occurs when reading the TcpStream at {} {}:{}", file!(), line!(), column!());
            continue;
        }
        let function_node: DifferentialFunctionNode = match rmp_serde::from_slice(&request) {
            Ok(function_node) => function_node,
            Err(e) => {
                warn!("Invalid request from client {:?} at {} {}:{}", e, file!(), line!(), column!());
                let _ = tcp_stream.shutdown(Shutdown::Both);
                continue;
            }
        };
        let cache = cache.lock().unwrap();
        match cache.get(&function_node) {
            Some(response) => respond(tcp_stream, response),
            None => computation_tx.send((function_node, tcp_stream)).or_log_err()
        }
    }

    Ok(())
}

fn respond(mut tcp_stream: TcpStream, msg: &Response) {
    match rmp_serde::to_vec(msg) {
        Ok(buffer) => tcp_stream.write_all(&buffer).or_log_err(),
        Err(e) => warn!("Serialisation error {:?} at {} {}:{}", e, file!(), line!(), column!())
    }
    let _ = tcp_stream.shutdown(Shutdown::Both);
}

fn run_computation_loop(rx: Receiver<(Request, TcpStream)>, cache: Arc<Mutex<OperatorInfoStore>>) {
    for (request, stream) in rx {
        {
            let cache = cache.lock().unwrap();
            if let Some(response) = cache.get(&request) {
                respond(stream, &response);
                continue;
            }
        }

        info!("Unknown function: {:?}. Computing the abstraction function.", &request);
        let response = compute_abstraction_of(&request);
        info!("Computed truncated Differential Function Node: {:?}", &response);
        {
            let mut cache = cache.lock().unwrap();
            cache.insert(request, response.clone());
        }
        respond(stream, &response);
    }
}

fn run_client(port: u16, diff_function: DifferentialFunctionNode) -> io::Result<()> {
    let mut client = TcpStream::connect(SocketAddr::from(([127, 0, 0, 1], port)))?;
    let mut buffer = rmp_serde::to_vec(&diff_function).unwrap();
    client.write_all(&buffer)?;
    let _ = client.shutdown(Shutdown::Write);
    buffer.clear();
    client.read_to_end(&mut buffer)?;
    let truncated_differential_function: TruncatedDifferentialFunctionNode = rmp_serde::from_slice(&buffer).unwrap();
    println!("{}", serde_json::ser::to_string(&truncated_differential_function).unwrap());
    Ok(())
}


fn main() -> io::Result<()> {
    let args = Cli::parse();
    match args.mode {
        Mode::Client(Client { port, differential_function }) => {
            let differential_function = serde_json::de::from_str(&differential_function)
                .unwrap();
            run_client(port, differential_function)
        }
        Mode::Server(Server { port }) => run_server(port)
    }
}
